package com.nextmatch.haultoy.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.nextmatch.haultoy.model.local.TodoData;

@Database(entities = {TodoData.class}, version = 2, exportSchema = false)
public abstract class TodoDatabase extends RoomDatabase {

    public abstract TodoDao todoDao();

    public static TodoDatabase instance = null;


    public static TodoDatabase getInstance(Context context){

        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), TodoDatabase.class, "Todo.db")
                    .fallbackToDestructiveMigration().build();
        }

        return instance;
    }

}
