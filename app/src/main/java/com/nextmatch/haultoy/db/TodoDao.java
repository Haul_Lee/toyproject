package com.nextmatch.haultoy.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.util.DateConverter;

import java.util.Date;
import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public interface TodoDao {

    @Query("SELECT * FROM todoTB WHERE todo_over_date < :today ORDER BY todo_over_date")
    List<TodoData> getPastTodoList(Date today);

    @Query("SELECT * FROM todoTB WHERE todo_over_date >= :today AND todo_over_date < :tomorrow ORDER BY todo_over_date")
    List<TodoData> getTodayTodoList(Date today, Date tomorrow);

    @Query("SELECT * FROM todoTB WHERE todo_over_date >= :tomorrow AND todo_over_date < :future ORDER BY todo_over_date")
    List<TodoData> getTomorrowTodoList(Date tomorrow, Date future);

    @Query("SELECT * FROM todoTB WHERE todo_over_date > :future ORDER BY todo_over_date")
    List<TodoData> getFutureTodoList(Date future);

    @Query("SELECT * FROM todoTB WHERE todo_complete == 1 ORDER BY todo_over_date")
    List<TodoData> getCompleteTodoList();

    @Query("SELECT * FROM todoTB WHERE todo_complete == 0 ORDER BY todo_over_date")
    List<TodoData> getNotCompleteTodoList();


    @Insert
    void insertTodoData(TodoData data);

    @Delete
    void deleteTodoData(TodoData data);

    @Update
    void updateTodoData(TodoData data);

    @Query("UPDATE todoTB SET todo_complete = 1 WHERE _id = :id")
    void completeTodo(int id);



}
