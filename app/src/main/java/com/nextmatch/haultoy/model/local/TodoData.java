package com.nextmatch.haultoy.model.local;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.nextmatch.haultoy.util.DateConverter;

import java.util.Date;

@Entity(tableName = "todoTB")
public class TodoData implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int _id;

    @ColumnInfo(name = "todo_title")
    @NonNull
    private String todo_title;

    @ColumnInfo(name = "todo_note")
    private String todo_note;

    @ColumnInfo(name = "todo_date")
    @TypeConverters({DateConverter.class})
    @NonNull
    private Date todo_date;

    @ColumnInfo(name = "todo_over_date")
    @TypeConverters({DateConverter.class})
    @NonNull
    private Date todo_over_date;

    @ColumnInfo(name = "todo_complete")
    @NonNull
    private int todo_complete;


    public TodoData(int _id, @NonNull String todo_title, String todo_note, @NonNull Date todo_date,
                    @NonNull Date todo_over_date, int todo_complete) {
        this._id = _id;
        this.todo_title = todo_title;
        this.todo_note = todo_note;
        this.todo_date = todo_date;
        this.todo_over_date = todo_over_date;
        this.todo_complete = todo_complete;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(_id);
        parcel.writeString(todo_title);
        parcel.writeString(todo_note);
        parcel.writeSerializable(todo_date);
        parcel.writeSerializable(todo_over_date);
        parcel.writeInt(todo_complete);
    }


    protected TodoData(Parcel in) {
        _id = in.readInt();
        todo_title = in.readString();
        todo_note = in.readString();
        todo_date = (Date)in.readSerializable();
        todo_over_date = (Date)in.readSerializable();
        todo_complete = in.readInt();
    }

    public static final Creator<TodoData> CREATOR = new Creator<TodoData>() {
        @Override
        public TodoData createFromParcel(Parcel in) {
            return new TodoData(in);
        }

        @Override
        public TodoData[] newArray(int size) {
            return new TodoData[size];
        }
    };

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    @NonNull
    public String getTodo_title() {
        return todo_title;
    }

    public void setTodo_title(@NonNull String todo_title) {
        this.todo_title = todo_title;
    }

    public String getTodo_note() {
        return todo_note;
    }

    public void setTodo_note(String todo_note) {
        this.todo_note = todo_note;
    }

    @NonNull
    public Date getTodo_date() {
        return todo_date;
    }

    public void setTodo_date(@NonNull Date todo_date) {
        this.todo_date = todo_date;
    }

    @NonNull
    public Date getTodo_over_date() {
        return todo_over_date;
    }

    public void setTodo_over_date(@NonNull Date todo_over_date) {
        this.todo_over_date = todo_over_date;
    }

    public int getTodo_complete() {
        return todo_complete;
    }

    public void setTodo_complete(int todo_complete) {
        this.todo_complete = todo_complete;
    }

}
