package com.nextmatch.haultoy.view.add;

import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.repository.AddRepository;

public class AddPresenter implements AddContract.Presenter, AddRepository.CallBack {

    private AddContract.View view;
    private AddRepository repository;


    public AddPresenter(AddContract.View view){
        this.view = view;
    }

    @Override
    public void onStartPresenter() {
        repository = new AddRepository(view.getActivityContext());
        repository.setCallBack(this);
    }

    @Override
    public void insertTodoData(TodoData data) {
        repository.insertTodoData(data);
    }

    @Override
    public void updateTodoData(TodoData data) {
        repository.updateTodoData(data);
    }

    @Override
    public void deleteTodoData(TodoData data) {
        repository.deleteTodoData(data);
    }

    @Override
    public void onSuccess(Object result) {
        view.finishView();
    }

    @Override
    public void onFailed() {

    }
}
