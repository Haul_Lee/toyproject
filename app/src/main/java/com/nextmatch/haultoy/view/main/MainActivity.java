package com.nextmatch.haultoy.view.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nextmatch.haultoy.R;
import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.util.FinalValue;
import com.nextmatch.haultoy.util.TodoWorker;
import com.nextmatch.haultoy.view.adapter.MainRecyclerAdapter;
import com.nextmatch.haultoy.view.add.AddActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements MainContract.View {


    private RecyclerView mainRecyclerView;
    private MainRecyclerAdapter mainRecyclerAdapter;

    private MainPresenter presenter;

    private int currentMode = FinalValue.MODE_TIME;

    private View.OnClickListener checkedChangeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            presenter.completeData((int)view.getTag());
        }
    };

    private View.OnClickListener moveEditActivityListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, AddActivity.class);
            intent.putExtra("todoData", (TodoData)view.getTag());

            startActivityForResult(intent, 1000);
        }
    };

    private View.OnClickListener deleteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            presenter.deleteTodoData((TodoData)view.getTag());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this);
        presenter.onStartPresenter();

        initView();

        presenter.getAllDataFromTime();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, AddActivity.class), 1000);
            }
        });

        mainRecyclerAdapter = new MainRecyclerAdapter();
        mainRecyclerAdapter.setCheckedChangeListener(checkedChangeListener);
        mainRecyclerAdapter.setTodoItemClickListener(moveEditActivityListener);
        mainRecyclerAdapter.setDeleteClickListener(deleteClickListener);

        mainRecyclerView = (RecyclerView)findViewById(R.id.recycler_main);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);
        mainRecyclerView.setLayoutManager(manager);

        mainRecyclerView.setAdapter(mainRecyclerAdapter);

//        OneTimeWorkRequest testWork = new OneTimeWorkRequest.Builder(TodoWorker.class)
//                .setInitialDelay(2, TimeUnit.MINUTES)
//                .build();
//
//        WorkManager.getInstance().enqueue(testWork);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (currentMode == FinalValue.MODE_TIME)
                presenter.getAllDataFromTime();
            else
                presenter.getAllDataFromComplete();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.sort_time) {
            currentMode = FinalValue.MODE_TIME;

            mainRecyclerAdapter.setCurrentMode(currentMode);
            presenter.getAllDataFromTime();

            return true;
        }else if(id == R.id.sort_complete){
            currentMode = FinalValue.MODE_COMPLETE;

            mainRecyclerAdapter.setCurrentMode(currentMode);
            presenter.getAllDataFromComplete();

            return true;
        }

        return false;
    }

    @Override
    public void completeGetAllTodo(HashMap<Integer, List<TodoData>> result) {
        mainRecyclerAdapter.setTodoItemMap(result);
        mainRecyclerAdapter.notifyDataSetChanged();
    }

}
