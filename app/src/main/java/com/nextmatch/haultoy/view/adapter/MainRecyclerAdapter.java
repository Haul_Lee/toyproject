package com.nextmatch.haultoy.view.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextmatch.haultoy.R;
import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.util.FinalValue;
import com.nextmatch.haultoy.view.custom.TodoItemView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.MainTodoViewHolder> {


    private String[] titleTime = {"지난", "오늘", "내일", "앞으로"};
    private String[] titleComplete = {"완료", "미완료"};
    private String[] positionPattern = {"yyyy.MM.dd a hh:mm", "a hh:mm"};

    private int currentMode = FinalValue.MODE_TIME;

    private HashMap<Integer, List<TodoData>> todoItemMap = new HashMap<>();
    private Context context;

    private View.OnClickListener checkedChangeListener;
    private View.OnClickListener deleteClickListener;
    private View.OnClickListener todoItemClickListener;

    @NonNull
    @Override
    public MainTodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_main_todo, parent, false);
        return new MainTodoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainTodoViewHolder holder, int position) {


        if(currentMode == FinalValue.MODE_TIME){
            holder.todoTitle.setText(titleTime[position]);
        }else{
            holder.todoTitle.setText(titleComplete[position]);
        }

        List<TodoData> itemList = todoItemMap.get(position);

        if(holder.todoListParent.getChildCount() > 0){
            holder.todoListParent.removeAllViews();
        }

        if(itemList == null || itemList.size() == 0){
            holder.todoListParent.setVisibility(View.GONE);

        }else{
            holder.todoListParent.setVisibility(View.VISIBLE);

            for(int i = 0; i < itemList.size(); i++){

                TodoData item = itemList.get(i);

                TodoItemView todoItemView = new TodoItemView(context);
                todoItemView.setCompleteCheckListener(checkedChangeListener, item.get_id());
                todoItemView.setDeleteClickListener(deleteClickListener, item);

                todoItemView.setTodoTitle(item.getTodo_title());
                todoItemView.setIsCompleteView(item.getTodo_complete() == 1);

                SimpleDateFormat dateFormat = null;

                if(currentMode == FinalValue.MODE_TIME){
                    dateFormat = new SimpleDateFormat((position == 1 || position == 2) ? positionPattern[1] : positionPattern[0]);

                }else{
                    dateFormat = new SimpleDateFormat(positionPattern[0]);
                }

                todoItemView.setTodoDate(dateFormat.format(item.getTodo_over_date()));
                todoItemView.setTag(item);
                todoItemView.setOnClickListener(todoItemClickListener);
                todoItemView.setClickable(item.getTodo_complete() != 1);

                holder.todoListParent.addView(todoItemView);
            }
        }
    }

    @Override
    public int getItemCount() {
        return currentMode == FinalValue.MODE_TIME ? 4 : 2;
    }


    public void setCurrentMode(int mode){
        currentMode = mode;
    }

    public void setTodoItemMap(HashMap<Integer, List<TodoData>> todoItemMap){
        this.todoItemMap = todoItemMap;
    }

    public void setCheckedChangeListener(View.OnClickListener checkedChangeListener){
        this.checkedChangeListener = checkedChangeListener;
    }

    public void setTodoItemClickListener(View.OnClickListener todoItemClickListener){
        this.todoItemClickListener = todoItemClickListener;
    }

    public void setDeleteClickListener(View.OnClickListener deleteClickListener){
        this.deleteClickListener = deleteClickListener;
    }


    public class MainTodoViewHolder extends RecyclerView.ViewHolder{

        public TextView todoTitle;
        public LinearLayout todoListParent;


        public MainTodoViewHolder(@NonNull View itemView) {
            super(itemView);

            todoTitle = (TextView) itemView.findViewById(R.id.main_todo_title);
            todoListParent = (LinearLayout)itemView.findViewById(R.id.main_todo_parent);

        }
    }

}
