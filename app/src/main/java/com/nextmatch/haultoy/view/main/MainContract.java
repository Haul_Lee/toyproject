package com.nextmatch.haultoy.view.main;

import com.nextmatch.haultoy.BasePresenter;
import com.nextmatch.haultoy.BaseView;
import com.nextmatch.haultoy.model.local.TodoData;

import java.util.HashMap;
import java.util.List;

public interface MainContract {

    interface View extends BaseView{

        void completeGetAllTodo(HashMap<Integer, List<TodoData>> result);
    }

    interface Presenter extends BasePresenter{
        void getAllDataFromTime();

        void getAllDataFromComplete();

        void completeData(int _id);

        void deleteTodoData(TodoData data);
    }
}
