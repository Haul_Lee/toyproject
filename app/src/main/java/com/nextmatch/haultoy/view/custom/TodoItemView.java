package com.nextmatch.haultoy.view.custom;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.nextmatch.haultoy.R;
import com.nextmatch.haultoy.model.local.TodoData;

public class TodoItemView extends ConstraintLayout {

    private CheckBox todoCompleteCheckView;
    private TextView todoTitleView;
    private TextView todoDateView;
    private View completeLineView;
    private Button todoDelete;


    public TodoItemView(Context context) {
        super(context);
        initView();
    }

    public TodoItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }


    public void initView(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_todo_list_item, this, false);

        addView(view);

        todoCompleteCheckView = (CheckBox) view.findViewById(R.id.todo_checkbox);
        todoTitleView = (TextView) view.findViewById(R.id.todo_title);
        todoDateView = (TextView) view.findViewById(R.id.todo_date);
        todoDelete = (Button) view.findViewById(R.id.todo_delete);

        completeLineView = (View) view.findViewById(R.id.complete_line_view);
    }

    public void setTodoTitle(String title){
        todoTitleView.setText(title);
    }


    public void setTodoDate(String date){
        todoDateView.setText(date);
    }

    public void setIsCompleteView(boolean isComplete) {
        completeLineView.setVisibility(isComplete ? VISIBLE : GONE);
        todoDateView.setVisibility(isComplete ? GONE : VISIBLE);
        todoCompleteCheckView.setChecked(isComplete);
        todoCompleteCheckView.setEnabled(!isComplete);
        todoCompleteCheckView.setClickable(isComplete);
    }

    public void setCompleteCheckListener(View.OnClickListener completeCheckListener, int _id){
        todoCompleteCheckView.setOnClickListener(completeCheckListener);
        todoCompleteCheckView.setTag(_id);
    }

    public void setDeleteClickListener(View.OnClickListener deleteClickListener, TodoData item){
        todoDelete.setOnClickListener(deleteClickListener);
        todoDelete.setTag(item);
    }
}
