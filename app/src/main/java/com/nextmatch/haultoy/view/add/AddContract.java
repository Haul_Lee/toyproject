package com.nextmatch.haultoy.view.add;

import com.nextmatch.haultoy.BasePresenter;
import com.nextmatch.haultoy.BaseView;
import com.nextmatch.haultoy.model.local.TodoData;

public interface AddContract {

    interface View extends BaseView{

        void initParsingExtras();

        void onClickCalender();

        void onClickTime();

        void onClickComplete();

        void onClickEdit();

        void onClickDelete();

        void finishView();
    }


    interface Presenter extends BasePresenter{

        void insertTodoData(TodoData data);

        void updateTodoData(TodoData data);

        void deleteTodoData(TodoData data);
    }

}
