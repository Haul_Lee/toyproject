package com.nextmatch.haultoy.view.add;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nextmatch.haultoy.R;
import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.util.FinalValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddActivity extends AppCompatActivity implements AddContract.View{

    private EditText todoInputTitle;
    private EditText todoInputNote;
    private TextView todoGoalDate;
    private TextView todoGoalTime;
    private Button btnCalender;
    private Button btnTime;
    private Button btnComplete;
    private Button btnEdit;
    private Button btnDelete;
    private LinearLayout editBtnParent;

    private AddPresenter presenter;

    private TodoData editTodoData;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        presenter = new AddPresenter(this);
        presenter.onStartPresenter();

        initView();
        initParsingExtras();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initView() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.top_bar_title_add));
        setSupportActionBar(toolbar);

        todoInputTitle = (EditText) findViewById(R.id.todo_input_title);
        todoInputNote = (EditText) findViewById(R.id.todo_input_note);

        todoGoalDate = (TextView) findViewById(R.id.todo_goal_date);
        todoGoalTime = (TextView) findViewById(R.id.todo_goal_time);

        Date nowDate = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat(FinalValue.PATTEN_DAY_DATETIME);
        todoGoalDate.setText(dateFormat.format(nowDate));

        SimpleDateFormat timeFormat = new SimpleDateFormat(FinalValue.PATTEN_TIME_DATETIME);
        todoGoalTime.setText(timeFormat.format(nowDate));

        btnCalender = (Button) findViewById(R.id.btn_calender_popup);
        btnCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCalender();
            }
        });

        btnTime = (Button) findViewById(R.id.btn_time_popup);
        btnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickTime();
            }
        });

        btnComplete = (Button) findViewById(R.id.btn_complete);
        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    onClickComplete();
            }
        });

        btnEdit = (Button) findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEdit();
            }
        });

        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickDelete();
            }
        });

        editBtnParent = (LinearLayout) findViewById(R.id.edit_btn_parent);

    }

    @Override
    public void initParsingExtras() {
        Intent intent = getIntent();

        if(intent.getExtras() != null){
            getSupportActionBar().setTitle(getResources().getString(R.string.top_bar_title_edit));

            editBtnParent.setVisibility(View.VISIBLE);
            btnComplete.setVisibility(View.GONE);

            editTodoData = intent.getParcelableExtra(FinalValue.INTENT_EXTRA_TODO_KEY);
            todoInputTitle.setText(editTodoData.getTodo_title());
            todoInputNote.setText(editTodoData.getTodo_note());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd/a hh:mm");
            String overDate = dateFormat.format(editTodoData.getTodo_over_date());

            todoGoalDate.setText(overDate.split("/")[0]);
            todoGoalTime.setText(overDate.split("/")[1]);
        }
        else{
            editBtnParent.setVisibility(View.GONE);
            btnComplete.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickCalender() {


        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                StringBuilder date = new StringBuilder();
                date.append(year).append(".");

                if(monthOfYear + 1 < 10)
                    date.append("0");

                date.append(monthOfYear + 1).append(".");

                if(dayOfMonth < 10)
                    date.append("0");

                date.append(dayOfMonth);

                todoGoalDate.setText(date.toString());
            }
        };

        String settingDate = todoGoalDate.getText().toString();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, listener,
                Integer.parseInt(settingDate.split("\\.")[0]),
                Integer.parseInt(settingDate.split("\\.")[1]) - 1,
                Integer.parseInt(settingDate.split("\\.")[2]));
        datePickerDialog.show();
    }

    @Override
    public void onClickTime() {
        TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                StringBuilder time = new StringBuilder();
                time.append(hour < 12 ? "오전" : "오후").append(" ");
                time.append(hour < 12 ? hour : hour - 12).append(":");

                if(minute < 10)
                    time.append("0");

                time.append(minute);

                todoGoalTime.setText(time.toString());
            }
        };

        String settingTime = todoGoalTime.getText().toString();
        String[] amPm = settingTime.split(" ");
        String[] timeStr = amPm[1].split(":");

        int hour = Integer.parseInt(timeStr[0]);
        int minute = Integer.parseInt(timeStr[1]);


        if(amPm[0].equals("오후")){
            hour += 12;
        }

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, listener, hour, minute, false);
        timePickerDialog.show();
    }

    @Override
    public void onClickComplete() {
        String title = todoInputTitle.getText().toString();
        String note = todoInputNote.getText().toString();
        String date = todoGoalDate.getText().toString();
        String time = todoGoalTime.getText().toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat(FinalValue.PATTEN_FULL_DATETIME);

        if(TextUtils.isEmpty(title)){
            Toast.makeText(this, "할일을 입력해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        try{
            presenter.insertTodoData(new TodoData(0, title, note, new Date(), dateFormat.parse(date + " " + time), 0));
        }catch (ParseException e){
            e.printStackTrace();

            Toast.makeText(this, "날짜와 시간 형식이 이상합니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

            return;
        }

    }

    @Override
    public void onClickEdit() {
        String title = todoInputTitle.getText().toString();
        String note = todoInputNote.getText().toString();
        String date = todoGoalDate.getText().toString();
        String time = todoGoalTime.getText().toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat(FinalValue.PATTEN_FULL_DATETIME);

        if(TextUtils.isEmpty(title)){
            Toast.makeText(this, "할일을 입력해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }


        try{

            editTodoData.setTodo_title(title);
            editTodoData.setTodo_note(note);
            editTodoData.setTodo_date(new Date());
            editTodoData.setTodo_over_date(dateFormat.parse(date + " " + time));
            editTodoData.setTodo_complete(0);

            presenter.updateTodoData(editTodoData);
        }catch (ParseException e){
            e.printStackTrace();

            Toast.makeText(this, "날짜와 시간 형식이 이상합니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

            return;
        }
    }

    @Override
    public void onClickDelete() {
        presenter.deleteTodoData(editTodoData);
    }


    @Override
    public void finishView() {
        setResult(RESULT_OK);
        finish();
    }
}
