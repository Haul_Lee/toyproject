package com.nextmatch.haultoy.view.main;

import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.repository.MainRepository;

import java.util.HashMap;
import java.util.List;

public class MainPresenter implements MainContract.Presenter, MainRepository.CallBack {

    private MainRepository repository;
    private MainContract.View view;

    // view 해제 코드가 없음


    // 생성자에서는 아무것도 받지 않게 왜냐하면 테스트 코드를 만들떄 편함
    public MainPresenter(MainContract.View view){
        this.view = view;
    }

    @Override
    public void onStartPresenter() {
        repository = new MainRepository(view.getActivityContext());
        repository.setCallBack(this);

    }

    @Override
    public void getAllDataFromTime() {
        repository.getAllDataFromTime();
    }

    @Override
    public void getAllDataFromComplete() {
        repository.getAllDataFromComplete();
    }

    // _ 는 사용 지향하기
    @Override
    public void completeData(int _id) {
        repository.completeData(_id);
    }

    @Override
    public void deleteTodoData(TodoData data) {
        repository.deleteTodoData(data);
    }

    @Override
    public void onSuccess(Object result) {
        // 한줄짜리 if문도 괄호
        if(result instanceof HashMap)
            view.completeGetAllTodo((HashMap<Integer, List<TodoData>>)result);
    }

    @Override
    public void onFailed() {

    }
}
