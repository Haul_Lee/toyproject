package com.nextmatch.haultoy.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.nextmatch.haultoy.BaseRepository;
import com.nextmatch.haultoy.db.TodoDatabase;
import com.nextmatch.haultoy.model.local.TodoData;
import com.nextmatch.haultoy.util.FinalValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainRepository extends BaseRepository {

    private Context context;
    private Date today;
    private Date tomorrow;
    private Date future;

    private boolean isModeTime = true;

    public MainRepository(Context context){
        this.context = context;
    }


    public void getAllDataFromTime(){

        isModeTime = true;

        SimpleDateFormat dateFormat = new SimpleDateFormat(FinalValue.PATTEN_DAY_DATETIME);
        String now = dateFormat.format(new Date());

        Calendar calendar = Calendar.getInstance();

        try {
            today = dateFormat.parse(now);

            calendar.setTime(today);

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tomorrow = calendar.getTime();

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            future = calendar.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }



        new AsyncTask<Void, Void, HashMap<Integer, List<TodoData>>>(){


            @Override
            protected HashMap<Integer, List<TodoData>> doInBackground(Void... voids) {

                HashMap<Integer, List<TodoData>> allTodoItemMap = new HashMap<>();

                allTodoItemMap.put(0, TodoDatabase.getInstance(context).todoDao().getPastTodoList(today));
                allTodoItemMap.put(1, TodoDatabase.getInstance(context).todoDao().getTodayTodoList(today, tomorrow));
                allTodoItemMap.put(2, TodoDatabase.getInstance(context).todoDao().getTomorrowTodoList(tomorrow, future));
                allTodoItemMap.put(3, TodoDatabase.getInstance(context).todoDao().getFutureTodoList(future));

                return allTodoItemMap;
            }


            @Override
            protected void onPostExecute(HashMap<Integer, List<TodoData>> results) {
                super.onPostExecute(results);

                if(results != null && results.size() > 0)
                    callBack.onSuccess(results);
                else
                    callBack.onFailed();
            }

        }.execute();

    }

    public void getAllDataFromComplete(){

        isModeTime = false;

        new AsyncTask<Void, Void, HashMap<Integer, List<TodoData>>>(){


            @Override
            protected HashMap<Integer, List<TodoData>> doInBackground(Void... voids) {

                HashMap<Integer, List<TodoData>> allTodoItemMap = new HashMap<>();

                allTodoItemMap.put(0, TodoDatabase.getInstance(context).todoDao().getCompleteTodoList());
                allTodoItemMap.put(1, TodoDatabase.getInstance(context).todoDao().getNotCompleteTodoList());

                return allTodoItemMap;
            }


            @Override
            protected void onPostExecute(HashMap<Integer, List<TodoData>> results) {
                super.onPostExecute(results);

                if(results != null && results.size() > 0)
                    callBack.onSuccess(results);
                else
                    callBack.onFailed();
            }

        }.execute();

    }



    public void completeData(int _id) {

        new AsyncTask<Integer, Void, Boolean>(){


            @Override
            protected Boolean doInBackground(Integer... ids) {

                try {
                    TodoDatabase.getInstance(context).todoDao().completeTodo(ids[0]);

                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return  false;
                }
            }


            @Override
            protected void onPostExecute(Boolean results) {
                super.onPostExecute(results);

                if(results)
                    if(isModeTime)
                        getAllDataFromTime();
                    else
                        getAllDataFromComplete();
                else
                    callBack.onFailed();
            }

        }.execute(_id);
    }


    public void deleteTodoData(TodoData data){
        new AsyncTask<TodoData, Void, Boolean>(){


            @Override
            protected Boolean doInBackground(TodoData... todoData) {

                try{

                    TodoDatabase.getInstance(context).todoDao().deleteTodoData(todoData[0]);
                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }

            }


            @Override
            protected void onPostExecute(Boolean complete) {
                super.onPostExecute(complete);

                if(complete)
                    if(isModeTime)
                        getAllDataFromTime();
                    else
                        getAllDataFromComplete();
                else
                    callBack.onFailed();
            }

        }.execute(data);
    }
}
