package com.nextmatch.haultoy.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.nextmatch.haultoy.BasePresenter;
import com.nextmatch.haultoy.BaseRepository;
import com.nextmatch.haultoy.db.TodoDatabase;
import com.nextmatch.haultoy.model.local.TodoData;

public class AddRepository extends BaseRepository {

    private Context context;

    public AddRepository(Context context){
        this.context = context;
    }


    public void insertTodoData(TodoData data){
        new AsyncTask<TodoData, Void, Boolean>(){


            @Override
            protected Boolean doInBackground(TodoData... todoData) {

                try{

                    TodoDatabase.getInstance(context).todoDao().insertTodoData(todoData[0]);
                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }

            }


            @Override
            protected void onPostExecute(Boolean complete) {
                super.onPostExecute(complete);

                if(complete)
                    callBack.onSuccess(null);
                else
                    callBack.onFailed();
            }

        }.execute(data);
    }


    public void updateTodoData(TodoData data){
        new AsyncTask<TodoData, Void, Boolean>(){


            @Override
            protected Boolean doInBackground(TodoData... todoData) {

                try{

                    TodoDatabase.getInstance(context).todoDao().updateTodoData(todoData[0]);
                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }

            }


            @Override
            protected void onPostExecute(Boolean complete) {
                super.onPostExecute(complete);

                if(complete)
                    callBack.onSuccess(null);
                else
                    callBack.onFailed();
            }

        }.execute(data);
    }


    public void deleteTodoData(TodoData data){
        new AsyncTask<TodoData, Void, Boolean>(){


            @Override
            protected Boolean doInBackground(TodoData... todoData) {

                try{

                    TodoDatabase.getInstance(context).todoDao().deleteTodoData(todoData[0]);
                    return true;
                }catch (Exception e){
                    e.printStackTrace();
                    return false;
                }

            }


            @Override
            protected void onPostExecute(Boolean complete) {
                super.onPostExecute(complete);

                if(complete)
                    callBack.onSuccess(null);
                else
                    callBack.onFailed();
            }

        }.execute(data);
    }

}
