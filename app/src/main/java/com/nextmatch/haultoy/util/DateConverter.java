package com.nextmatch.haultoy.util;

import android.text.TextUtils;

import androidx.room.TypeConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long time){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd a hh:mm");

        try {
            return time == null ? null : dateFormat.parse(dateFormat.format(new Date(time)));
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }

    }

    @TypeConverter
    public static Long fromDate(Date timeDate){
        return timeDate == null ? null : timeDate.getTime();
    }
}
