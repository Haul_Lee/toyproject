package com.nextmatch.haultoy.util;

public class FinalValue {


    public static final int MODE_TIME = 0;
    public static final int MODE_COMPLETE = 1;

    public static final String INTENT_EXTRA_TODO_KEY = "todoData";

    public static final String PATTEN_FULL_DATETIME = "yyyy.MM.dd a hh:mm";
    public static final String PATTEN_DAY_DATETIME = "yyyy.MM.dd";
    public static final String PATTEN_TIME_DATETIME = "a hh:mm";


}
