package com.nextmatch.haultoy;

import android.content.Context;

public interface BaseView {

    Context getActivityContext();

    void initView();

}
