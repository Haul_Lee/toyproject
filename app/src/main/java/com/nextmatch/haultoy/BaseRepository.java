package com.nextmatch.haultoy;

public class BaseRepository {

    public interface CallBack<T> {
        void onSuccess(T result);
        void onFailed();
    }

    protected CallBack callBack;

    public void setCallBack(CallBack callBack){
        this.callBack = callBack;
    }
}
